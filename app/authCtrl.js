randsagaApp.controller('authCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {

    $scope.login = {};
    $scope.signup = {};
    $scope.update = {};

    $scope.doLogin = function (user) {
        Data.post('login', {
            user: user
        }).then(function (results) {
            Data.toast(results);
            console.log(results)
            if (results.status == "success") {
                $location.path('main');
            }
        });
    };

    $scope.updatePW = function (password) {
        Data.post('updatePW', {
            password: [password, $routeParams.pwcode]
        }).then(function (results) {
            Data.toast(results);

            if(results.status == "success"){
                $location.path('login');
            }
        });
    };

    $scope.forgotPW = function (user) {
        Data.post('forgotPW', {
            user: user
        }).then(function (results) {
            Data.toast(results);
            if(results.status == "success"){
                
            }
        });
    };

    $scope.signup = {email:'', username:'', password:''};
    $scope.signUp = function (user) {
        Data.post('signUp', {
            user: user
        }).then(function (results) {
            console.log(results);
            Data.toast(results);
            if (results.status == "success") {
                $location.path('profile');
            }
        });
    };

    $scope.logout = function () {
        Data.get('logout').then(function (results) {
            Data.toast(results);
            $location.path('home');
        });
    }
});