randsagaApp.controller('sidebarCtrl', function ($scope, $location, $routeParams) {

    $scope.leftSidebar = '';
    $scope.rightSidebar = '';
    $scope.centerView = '';

    switch ($location.path())
    {
        case '/story/' + $routeParams.sid:
            $scope.leftSidebar = 'html/sidebars/story_profile_left.html';
            $scope.rightSidebar = 'html/sidebars/story_profile_right.html';
            $scope.centerView = 'html/centerviews/story.html';
            break;
        case '/story_internal':
            $scope.leftSidebar = 'html/empty.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/story_internal.html';
            break;
        case '/profile':
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/profile_user.html';
            break;
        case '/main':
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/sidebars/social.html';
            $scope.centerView = 'html/centerviews/main.html';
            break;
        case '/community':
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/community.html';
            break;
        case '/messages':
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/messages.html';
            break;
        case '/search':
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/search.html';
            break;
        case '/library':
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/library.html';
            break;
        case '/createStory':
            $scope.leftSidebar = 'html/empty.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/create_story.html';
            break;
        case '/story/' + $routeParams.sid + '/' + $routeParams.bid:
            $scope.leftSidebar = 'html/empty.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/branch_view.html';
            break;
        case '/users/' + $routeParams.uid:
            $scope.leftSidebar = 'html/sidebars/default.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/centerviews/profile_users.html';
            break;
        default:
            $scope.leftSidebar = 'html/empty.html';
            $scope.rightSidebar = 'html/empty.html';
            $scope.centerView = 'html/empty.html';
    }

    }

);
