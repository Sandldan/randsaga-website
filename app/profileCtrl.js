randsagaApp.controller('profileCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data, toaster) {
    $scope.editingEmail = false;
    $scope.editingDesc = false;
    $scope.editingWebsite = false;

    function getProfile(){
        Data.get('session').then(function (results) {
            if (results.uid) {
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.username = results.username;
                $rootScope.email = results.email;
                $rootScope.desc = results.description;
                $rootScope.website = results.website;
            }
        });
    }

    $scope.clearEditing = function () {
        if ($scope.editingEmail) {
            $scope.editingEmail = false;
        }

        if ($scope.editingWebsite) {
            $scope.editingWebsite = false;
        }

        if ($scope.editingDesc) {
            $scope.editingDesc = false;
        }
    }

    $scope.toggleEditEmail = function () {
        $scope.editingEmail = !$scope.editingEmail;
    };

    $scope.toggleEditDesc = function () {
        $scope.editingDesc = !$scope.editingDesc;
    };

    $scope.toggleEditWebsite = function () {
        $scope.editingWebsite = !$scope.editingWebsite;
    };

    $scope.updateField = function (input) {
        Data.post('updateField', {
            input: input,
            uid: $scope.uid
        }).then(function (results) {
        	Data.toast(results);
            if (results.status == "success") {
            	getProfile();
            }
        });
    };

    $scope.logout = function () {
        Data.get('logout').then(function (results) {
            Data.toast(results);
            $rootScope.authenticated = false;
            $location.path('/');
        });
    }

});