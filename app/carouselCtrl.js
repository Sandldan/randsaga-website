﻿randsagaApp.controller('carouselCtrl', ['$scope', function ($scope) {

    $('.carousel').carousel({
        interval: 5000,
        keyboard: true
    })

    $scope.carouselPrev = function () {
        $($scope.target).carousel('prev')
    }

    $scope.carouselNext = function () {
        $($scope.target).carousel('next')
    }

    $scope.carouselGoto = function (slideNr) {
        $($scope.target).carousel(slideNr)
    }
}]);