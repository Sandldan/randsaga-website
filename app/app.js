﻿var randsagaApp = angular.module('randsagaApp', ['ngRoute', 'ngAnimate', 'toaster', 'textAngular']);

randsagaApp.config(['$routeProvider', '$locationProvider',
  function ($routeProvider, $locationProvider) {

      $routeProvider
      .when('/register', {
            title: 'Register',
            templateUrl: 'Forms/register.html',
            controller: 'authCtrl'
        })

        .when('/login', {
            title: 'Login',
            templateUrl: 'Forms/login.html',
            controller: 'authCtrl'
        })

        .when('/logout', {
            title: 'Logout',
            templateUrl: 'html/start.html'
        })

        .when('/', {
            templateUrl: 'html/start.html'
        })

        .when('/main', {
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl'
        })

        .when('/home', {
            title: 'Start',
            templateUrl: 'html/start.html',
            controller: 'authCtrl'
        })

        .when('/pwreset/:pwcode', {
            title: 'Reset password',
            templateUrl: 'Forms/pwcode.html',
            controller: 'authCtrl'
        })

        .when('/forgotpw', {
            title: 'Recover Password',
            templateUrl: 'Forms/forgotpw.html',
            controller: 'authCtrl'
        })

        .when('/info', {
            title: 'Info',
            templateUrl: 'html/info.html',
            controller: 'profileCtrl',
            requireLogin: true
        })

        .when('/createStory', {
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl',
            requireLogin: true
        })

        .when('/profile', {
            title: 'Profile',
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl',
            requireLogin: true
        })

        .when('/story_internal', {
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl'
        })

        .when('/search', {
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl'
        })
        .when('/library', {
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl',
            requireLogin: true
        })
        .when('/messages', {
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl',
            requireLogin: true
        })

        .when('/story/:sid', { 
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl'
        })

        .when('/users/:uid', { 
            templateUrl: 'html/sidebarview.html',
            controller: 'sidebarCtrl'
        })

        .when('/story/:sid/createBranch', {
            templateUrl: 'Forms/create_branch.html',
            controller: 'branchCtrl',
            requireLogin: true
        })

        /*        
        .when('/story/:sid/:bid', {
          templateUrl: 'html/branch_view.html',
          controller: 'branchCtrl'
        })*/

        .when('/story/:sid/:bid', {
          templateUrl: 'html/sidebarview.html',
          controller: 'sidebarCtrl'
        })

        .otherwise({
            redirectTo: '/'
        });

        $locationProvider.html5Mode(false).hashPrefix("!");
  }])

    .run(function ($rootScope, $location, Data, $route) {
        $rootScope.$on("$routeChangeStart", function (event, next, current) {
            Data.get('session').then(function (results) {
                if (results.uid) {
                    $rootScope.authenticated = true;
                    $rootScope.uid = results.uid;
                    $rootScope.username = results.username;
                    $rootScope.email = results.email;
                    $rootScope.desc = results.description;
                    $rootScope.website = results.website;
                    $rootScope.avatar = results.avatar;

                } else {
                    var requireLogin = $route.current.$$route.requireLogin;

                    if (requireLogin === true) {
                      $location.path("/login");
                    }
                }
            });
        });
    });
