randsagaApp.controller('branchCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {


    function getUser(){
        Data.get('session').then(function (results) {
            if (results.uid) {
                $rootScope.authenticated = true;
                $rootScope.uid = results.uid;
                $rootScope.username = results.username;
                //console.log(results.result);
            }
        });
    }

    function getBranch(){
        Data.get('viewBranch/' + $routeParams.bid).then(function (results){
            if (results.status == "success"){
                $rootScope.branchTitle = results.result.head;
                $rootScope.branchIntroText = results.result.body;
                $rootScope.branchChoice = results.result.choice;
                $rootScope.parent = results.result.parent;
                $rootScope.sid = results.result.sid;
                $rootScope.bid = results.result.id;

                console.log(results.result);
            }
        });
    }

    function getBranchChildren(){
        Data.get('getChildren/' + $routeParams.bid ,  {
        }).then(function (results) {
            //Data.toast(results);
            if (results.status == "success"){
                $scope.branchChoices = results.result;
                $scope.branchhead = results.result.head;
                console.log(results.result);
            }
        });
    };

    $scope.updateField = function (input) {
        Data.post('updateBranch/' + $routeParams.bid, {
            input: input,
        }).then(function (results) {
            Data.toast(results);
            if (results.status == "success") {
                getBranch();
                getBranchChildren();
            }
        });
    };

    $scope.branch = {choice:''};
    $scope.createBranch = function(branch){
        if ($scope.branchChoices.length > 4) return;
        Data.post('createBranch/' + $routeParams.sid + '/' + $routeParams.bid , {
            branch: branch
        }).then(function (results) {
            //Data.toast(results);
            console.log(results);
            if (results.status == "success"){
                getBranchChildren();
            }
        });

    };

    $scope.deleteBranch = function(id){
        Data.delete('deleteBranch/' + id ,{
        }).then(function (results) {
            //console.log(results);
            getBranchChildren();
        });
    };

/*    function getChoices(){
        Data.get('')
    }*/

    $scope.init = function ()
    {
        getUser();
        getBranch();
        getBranchChildren();

        //$scope.branchID = -1; // ID of this branch/scope
        //$scope.branchAuthorID = -1; // Who wrote it

        $scope.branchChoices = [ // example choice array, null == create new branch, otherwise exists: link to that branch
/*            { 'link_id': null, 'header': 'Choice 1' },
            { 'link_id': 1337, 'header': 'Choice 2' },
            { 'link_id': null, 'header': 'Choice 3' }*/
        ];

        //$scope.branchIntroText = "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. \n\nEt harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.";
        //$scope.branchTitle = "Branch Title";

        $scope.editMode = false; // Are we editing?
        $scope.editRights = $scope.authenticated; //defaults to auth, fix this when/if branch creator/isLeafBranch can be checked for. logged in user == branch creator

        $scope.maxedChoices = $scope.branchChoices.length < 5;
    }

    $scope.toggleEdit = function ()
    {
        $scope.editMode = !$scope.editMode;
    }

    $scope.cancelEdit = function () {
        //TODO: Reset fields.
            $scope.editMode = false;
    }

    $scope.getChoices = function ()
    {
        return $scope.branchChoices;
    }

    $scope.addChoice = function () {
        if ($scope.branchChoices.length > 4) return; //disallow more than 5 choices.
        $scope.branchChoices.splice($scope.branchChoices.length, 0, { 'link_id': null, 'header': 'New Branch' });
        $scope.maxedChoices = $scope.branchChoices.length < 5;
    }

    $scope.removeChoice = function(index) {
        $scope.branchChoices.splice(index, 1);
        $scope.maxedChoices = $scope.branchChoices.length < 5;
    }

    $scope.publish = function () {
        // save choice array (link_id,text), title and intro text to database for this branch
        $scope.editMode = false;
        //updateField(update1);
    }

});
