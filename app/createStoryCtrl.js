randsagaApp.controller('createStoryCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data) {



    $scope.rootChoices = [];

    $scope.story = {
        'storyName': '',
        'storyCategory': '',
        'storyDesc': ''
    };

    $scope.rootBranch = {};

    $scope.publish = function () {
        //flip published boolean in db in story and rootbranch to true;
        updateRootBranch();
        for (i = 0; i < $scope.rootChoices.length; i++)
        {
            updateChild($scope.rootChoices[i]);
        }

        $location.path('/story/' + $scope.rootBranch.sid);
    }


    //$scope.createStory = {storyname:'', category:'', uid:'', description:'' , sid:''}; //dafuq is this? doesn't match the backend. Is it old? - scandoli

    $scope.createStory = function(story){ 

        Data.post('createStory', {
            story: story
        }).then(function (results, sid) {
            if (results.status == "success") {
                $scope.unpubStory = results.result;
                getRootBranch(results.sid);
            }
        });
    };

    function getRootBranch(sid) {
        Data.get('rootBranch/' + sid, {
        }).then(function (results) {
            if (results.status == "success") {
                $scope.rootBranch = results.result;
            }
        });
    };

    function getRootChildren() {
        Data.get('getChildren/' + $scope.rootBranch.id, {
        }).then(function (results) {
            if (results.status == "success") {
                $scope.rootChoices = results.result;
                //console.log(results.result);
            }
        });
    };

    $scope.createBranch = function (branch) {
        if ($scope.rootChoices.length > 4) return;
        branch: branch;
        Data.post('createBranch/' + $scope.rootBranch.sid + '/' + $scope.rootBranch.id, {
        }).then(function (results) {
            console.log(results);
            if (results.status == "success") {
                //do something
                getRootChildren();
            }
        });

    };

    $scope.deleteBranch = function (id) {
        Data.delete('deleteBranch/' + id, {
        }).then(function (results) {
            getRootChildren();
        });
    };


    function updateRootBranch() {
        Data.post('updateBranch/' + $scope.rootBranch.id, {
            input: $scope.rootBranch,
            branch: $scope.rootBranch.id
        }).then(function (results) {
            if (results.status == "success") {
                //do something
            }
        });
    };

    function updateChild(child) {
        Data.post('updateBranch/' + child.id, {
            input: child,
            branch: child.id
        }).then(function (results) {
            if (results.status == "success") {
                //do something
            }
        });
    };



    // TAB EVENTS


    $scope.$on('tabAttemptPrev', function (event, currentTab) {

        $scope.$emit('tabAllowPrev', true);
    });

    $scope.$on('tabAttemptNext', function (event, currentTab) {

        if (currentTab == 'createStory1') // step 1
        {
            if ($scope.story.storyName.length <= 0 ||
                $scope.story.storyCategory.length <= 0 ||
                $scope.story.storyDesc.length <= 0)
            {
                window.alert("You must fill in all fields before you can continue! ");
                $scope.$emit('tabAllowNext', false);
                return;
            }

            // passed test, create story
            var story = { name: $scope.story.storyName, category: $scope.story.storyCategory, description: $scope.story.storyDesc };
            $scope.createStory(story);
        }

        if (currentTab == 'createStory2') // step 2
        {

        }

        $scope.$emit('tabAllowNext', true);
    });

});