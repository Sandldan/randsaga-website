randsagaApp.controller('getCtrl', function ($scope, Data, $rootScope, $routeParams, $location) {

    $scope.url = $location.absUrl();
    $scope.identifier = $routeParams.sid;

    $scope.updateSearch = function () {

        var string = $location.url();
        var array = string.split("?");

        if (array.length > 1)
        {
            $scope.searchQuery = array[1];
            $scope.searchStories();
            $scope.searchUsers();
        }
    }


    $scope.getOnLoadStories = function(){
        Data.get('getOnLoadStories', {
        }).then(function (results) {
            if (results.status == "success"){
                $scope.stories = results.result;
            }
        });
    };

    $scope.searchStories = function(){
        if (!$scope.searchQuery){
            $scope.getOnLoadStories()
        }
        else {
        Data.post('searchStories', {
            query: $scope.searchQuery
        }).then(function (results) {
            if (results.status == "success"){
                $scope.stories = results.result;
            } else {
                $scope.stories = [];
            }
        });
        }
    };

    $scope.searchUsers = function(){
        Data.post('searchUsers', {
            query: $scope.searchQuery
        }).then(function (results) {
            if (results.status == "success"){
                $scope.users = results.result;
            } else {
                $scope.users = [];
            }
        });
    }

    $scope.getStoryInfo = function(){
        Data.get('getStoryInfo/' + $routeParams.sid  ,  {
        }).then(function (results) {
            if (results.status == "success"){
                $scope.stories = results.result;
            }
        });
    };

    $scope.getRootBranch = function(){
        Data.get('rootBranch/' + $routeParams.sid , {
        }).then(function (results) {
            if (results.status == "success"){
                $scope.root = results.result;
            }
        });
    };

    $scope.removeFormatting = function (string) {
        var tempDiv = document.createElement("DIV");
        tempDiv.innerHTML = string;
        return tempDiv.innerText;
    };


});