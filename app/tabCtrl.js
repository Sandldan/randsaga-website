﻿randsagaApp.controller('tabCtrl', function ($scope, $location) {

    $scope.curTab = 0;

    $scope.cancel = function () {
        //Discard everything, just go back to library
        $location.path('library');
    }

    $scope.focusTab = function (tabNr) {
        $('#tab' + tabNr).tab('show');
        $scope.curTab = tabNr;
    }

    $scope.changeView = function (viewName)
    {
        switch (viewName)
        {

            //story creation view
            case 'createStory1':
                $scope.currentView = 'html/tabviews/create_story_first.html';
                $scope.currentTabs = 'html/tabs/createStory.html';
                $scope.focusTab(0);
                break;
            case 'createStory2':
                $scope.currentView = 'html/tabviews/create_story_second.html';
                $scope.currentTabs = 'html/tabs/createStory.html';
                $scope.focusTab(1);
                break;
            case 'createStory3':
                $scope.currentView = 'html/tabviews/create_story_third.html';
                $scope.currentTabs = 'html/tabs/createStory.html';
                $scope.focusTab(2);
                break;
            default: // do nothing
        }

    }

    $scope.$on('tabAllowPrev', function (event, allow) {
        if ($scope.curTab == 0 || !allow)
            return;

        $scope.changeView($scope.availableViews[$scope.curTab - 1])
    });

    $scope.$on('tabAllowNext', function (event, allow) {

        if ($scope.curTab == $scope.availableViews.length || !allow)
            return;

        $scope.changeView($scope.availableViews[$scope.curTab + 1])
    });

    $scope.prevView = function () {
        $scope.$broadcast('tabAttemptPrev', $scope.availableViews[$scope.curTab]);
    }

    $scope.nextView = function () {
        $scope.$broadcast('tabAttemptNext', $scope.availableViews[$scope.curTab]);
    }


    switch ($location.path()) {
        case '/createStory':
            $scope.availableViews = ['createStory1', 'createStory2', 'createStory3'];
            $scope.changeView('createStory1');
            break;
        default:
            $scope.availableViews = ['createStory1', 'createStory2', 'createStory3'];
            $scope.changeView('createStory1');
    }

})