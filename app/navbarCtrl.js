﻿randsagaApp.controller('navbarCtrl', function ($scope, Data, $location, $rootScope) {

    var queryStories;
    var queryPeople;

    $scope.initNavbar = function ()
    {
        $scope.showBrand = false;
        $scope.showSearch = false;
        $scope.brandlink = '#/';
        $scope.showTypeahead = false;
    }

    function viewChanged() {
        $scope.navbarUpdate();
    }

    $scope.navbarUpdate = function ()
    {
        $scope.showTypeahead = false;

        switch ($location.path()) {
            case '/':
                $scope.showBrand = false;
                $scope.showSearch = false;
                break;
            case '/search':
                $scope.showBrand = true;
                $scope.showSearch = false;
                break;
            default:
                $scope.showBrand = true;
                $scope.showSearch = true;
        }
    }


    $scope.filterStories = function (searchQuery) {
        Data.post('searchStories', {
            query: searchQuery
        }).then(function (results) {
            if (results.status == "success") {
                queryStories = results.result;
            } else {
                queryStories = [];
            }

            updateTypeahead();
        });
    };

    $scope.filterUsers = function (searchQuery) {
        Data.post('searchUsers', {
            query: searchQuery
        }).then(function (results) {
            if (results.status == "success") {
                queryPeople = results.result;
            } else {
                queryPeople = [];
            }
        });
    }



    $scope.searchRedirect = function (searchQuery)
    {
        if ($location.path == 'search')
            return;

        $location.path('search');

        if(searchQuery != null && searchQuery.length > 0)
            $location.search(searchQuery);
    }


    $scope.removeFormatting = function (string) {
        var tempDiv = document.createElement("DIV");
        tempDiv.innerHTML = string;
        return tempDiv.innerText;
    };

    function updateTypeahead()
    {
        $scope.queryStories = [];
        $scope.queryPeople = [];

        if (queryStories != null && queryStories.length > 0) {
            $scope.showTypeahead = true;
            $scope.queryStories = queryStories;
        }

        if (queryPeople != null && queryPeople.length > 0)
        {
            $scope.showTypeahead = true;
            $scope.queryPeople = queryPeople;
        }

        if (!(queryPeople != null && queryPeople.length > 0) && !(queryStories != null && queryStories.length > 0)) {
            $scope.showTypeahead = false;
        }

    }

    $scope.$on('$routeChangeUpdate', viewChanged);
    $scope.$on('$routeChangeSuccess', viewChanged);

    $scope.initNavbar();

}

);
