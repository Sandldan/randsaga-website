randsagaApp.controller('userCtrl', function ($scope, $rootScope, $routeParams, $location, $http, Data, toaster) {

    $scope.url = $location.absUrl();
    $scope.identifier = $routeParams.uid;

    $scope.getUserProfile = function(){
        Data.post('getProfile', {
            username: $routeParams.uid
        }).then(function (results) {
            if (results.status == "success"){
                $scope.user = results.result;
            }
            //User does not exist
            else {
                $scope.user = false;
            }
        });
    }



});